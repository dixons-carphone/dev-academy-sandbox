# Dixons Dev Academy Sandbox

## Requirements

* Composer (https://getcomposer.org/doc/00-intro.md)
* PHP 5.5+

## Courses

### Twig

* http://slides.com/itdevdixons/twig?token=isuCgS5BXMypxjp8UmDqCpp4fJwr

#### Nix

    # go to home directory
    cd courses/twig
    # install libraries
    composer install
    # go to web root directory
    cd web
    # run PHP web server
    php -S localhost:5000
    # navigate to homepage
    open http://localhost:5000/index.html

#### Windows


