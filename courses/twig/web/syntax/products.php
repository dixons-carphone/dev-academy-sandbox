<?php

use Dixons\DevAcademy\Eshop\Model\Product;

require_once __DIR__ . '/../../vendor/autoload.php';

// Load e-shop configuration
$config = require __DIR__ . '/../../config/config.php';

// Prepare our products
$products = array(
    new Product('Mega TV', 323),
    new Product('Acer 4000', 400),
    new Product('Samsung Teleport 2', 8999)
);

// Bootstrap Twig
$loader = new Twig_Loader_Filesystem(__DIR__ . '/../../src/Dixons/DevAcademy/Eshop/Resources/views');
$twig = new Twig_Environment($loader, array(
    'debug' => true,
    'cache' => __DIR__ . '/../../var/cache/twig',
));

// Render template
echo $twig->render('syntax/products.html.twig', array(
    'eshopName' => $config['eshop_name'],
    'products' => $products
));


