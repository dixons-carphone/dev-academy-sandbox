<?php

use Dixons\DevAcademy\Eshop\Model\Product;

require_once __DIR__ . '/../../vendor/autoload.php';

// Load e-shop configuration
$config = require __DIR__ . '/../../config/config.php';

// Prepare our products
$product1 = new Product('Mega TV', 323);

$product2 = new Product('Acer 4000', 400, '', true);
$product2->comment('Ingrid', 'Slow.');
$product2->comment('Thomas', 'Fast.');

$product3 = new Product('Teleport 2021', 323);
$product3->comment('Miroslav', 'Where am I?');
$product3->comment('Josephine', 'Missing manual');

$products = array($product1, $product2, $product3);

// Bootstrap Twig
$loader = new Twig_Loader_Filesystem(__DIR__ . '/../../src/Dixons/DevAcademy/Eshop/Resources/views');
$twig = new Twig_Environment($loader, array(
    'debug' => true,
    'cache' => __DIR__ . '/../../var/cache/twig',
));

// Render template
echo $twig->render('control/products.html.twig', array(
    'eshopName' => $config['eshop_name'],
    'products' => $products
));


