<?php

namespace Dixons\DevAcademy\Eshop\Model;

/**
 * Product.
 */
class Product
{
    /**
     * Product title
     *
     * @var string
     */
    protected $title;

    /**
     * Product price
     *
     * @var int
     */
    protected $price;

    /**
     * Product specification
     *
     * @var string|null
     */
    protected $specification;

    /**
     * Product in sale
     *
     * @var bool
     */
    protected $inSale;

    /**
     * Product comments.
     *
     * @var array
     */
    protected $comments;
    /**
     * Constructor.
     *
     * @param string $title
     * @param int $price
     * @param null|string $specification
     * $param bool $inSale
     */
    public function __construct($title, $price = 0, $specification = null, $inSale = false)
    {
        $this->title = $title;
        $this->price = $price;
        $this->specification = $specification;
        $this->inSale = $inSale;
        $this->comments = [];
    }

    /**
     * Returns title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Returns specification.
     *
     * @return null|string
     */
    public function getSpecification()
    {
        return $this->specification;
    }

    public function isInSale()
    {
        return $this->inSale;
    }

    /**
     * Returns comments.
     *
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Comment product.
     *
     * @param string $author
     * @param string $comment
     */
    public function comment($author, $comment)
    {
        $this->comments[] = new Comment($this, $author, $comment);
    }
}