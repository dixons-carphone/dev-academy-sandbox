<?php

namespace Dixons\DevAcademy\Eshop\Model;

/**
 * Comment.
 */
class Comment
{
    /**
     * Commented Product
     *
     * @var Product
     */
    protected $product;

    /**
     * Comment author
     *
     * @var string
     */
    protected $author;

    /**
     * Comment
     *
     * @var string
     */
    protected $comment;

    /**
     * Constructor.
     *
     * @param Product $product
     * @param string $author
     * @param string $comment
     */
    public function __construct(Product $product, $author, $comment)
    {
        $this->product = $product;
        $this->author = $author;
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->comment;
    }

    /**
     * Returns product.
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Returns author.
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Returns comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}